import React from "react";
import { StyleSheet, View } from "react-native";
import { CharactersContext } from "./AuthNavigator";
import CharactersDisplay from "../components/characterSelector/charactersDisplay";




export default function Screen(props) {
    const styles = StyleSheet.create({
    })
    let idx = props.characters.characters.findIndex(el=>el['active']==true);
    if(idx==-1){
        idx=0;
    }
    props.characters.characters.forEach((el,i) => {
        if(i!=idx){
           delete el['active'];
        }else{
            el['active']=true;
        }
    });
    console.log(props.characters.characters,idx);
    return (
    <View style={{flex:1}}>
        <CharactersContext.Consumer>
            {el=>{<CharactersDisplay characters={el}></CharactersDisplay>}}
        </CharactersContext.Consumer>
        {props.navigator}
    </View>
    )
}