import React,{ useState}  from 'react';
import AppNavigator from "./AppNavigator";
import { AsyncStorage,ImageBackground, SafeAreaView } from 'react-native';
import { StyleSheet} from 'react-native';
import LoginScreen from '../screens/LoginScreen';

import firebase from "firebase"
import { Color,colorOpacity } from '../constants/Colors';
import Screen from './Screen';
require("firebase/firestore");


export  const CharactersContext = React.createContext();
const background = require("../assets/images/resources/background.jpg")
export default function AuthNavigator(props) {
    const [loggin,setLoggin] = useState({characters:null,isLogged:false});
    if(!loggin.isLogged){
        AsyncStorage.getItem("token")
        .then(r=>{if(r!=null){
            let userId = JSON.parse(r).id;
            AsyncStorage.getItem("characters")
                .then(localCharacter=>{
                    if(localCharacter==undefined || localCharacter==null){
                        firebase.firestore().collection("characters").doc(userId).get()
                                .then(r=>{
                                    if(r.data()!=undefined){
                                    AsyncStorage.setItem("characters",JSON.stringify(r.data()))
                                    setLoggin({characters:r.data(),isLogged:true});
                                    }else{
                                        let newCharacter = {
                                            "characters":[{"id":1,"name":""}],
                                            "last":1
                                        }
                                        AsyncStorage.setItem("characters",JSON.stringify(newCharacter.data()))
                                        setLoggin({characters:newCharacter,isLogged:true});
                                    }
                                })
                    }else{
                        setLoggin({characters:JSON.parse(localCharacter),isLogged:true});
                    }
                })
        }})
    }
    if (!loggin.isLogged) {
        return (
            <LoginScreen setLoggin={setLoggin}></LoginScreen>
        );
    } else {
        return (
            <ImageBackground source={background} style={styles.backGround} imageStyle={styles.backGroundImage}>
                <SafeAreaView style={styles.container}>
                    {/* <CharactersContext.Provider value={loggin.characters}>
                        <Screen characters={loggin.characters} navigator={<AppNavigator/>}/>
                    </CharactersContext.Provider> */}
                </SafeAreaView>
            </ImageBackground>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:colorOpacity(Color.red,0),
    },
    backGround:{
        flex:1,
        width: '100%',
        height: '100%',
    }
  });