import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import DicesScreen from '../screens/DicesScreen';
import CharacterScreen from '../screens/CharacterScreen';

// const config = Platform.select({
//   web: { headerMode: 'screen' },
//   default: {
//     headerMode:"none",
//     cardStyle:{
//       backgroundColor:"rgba(0,0,0,0)"
//     }
//   },
// });

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  {
    cardStyle:{
      mode:"modal",
      backgroundColor:"red"
    }
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: undefined,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
  {
    Links: LinksScreen,
  },
  {
    cardStyle:{
      mode:"modal",
      backgroundColor:"rgba(0,0,0,0)"
    }
  }
);

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
  ),
};

LinksStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  {
    cardStyle:{
      mode:"modal",
      backgroundColor:"rgba(0,0,0,0)"
    }
  }
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
  ),
};

SettingsStack.path = '';

const testStack = createStackNavigator (
  {
    dices:DicesScreen,
  },
  {
    cardStyle:{
      mode:"modal",
      backgroundColor:"rgba(0,0,0,0)"
    }
  }
);

testStack.navigationOptions = {
  tabBarLabel: 'Dados',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
  ),
  
}

const characterScreen = createStackNavigator(
  {
    character: CharacterScreen,
  },
  {
    cardStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
    },
  }
)
characterScreen.navigationOptions = {
  tabBarLabel:"Personaje",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
    )
}

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  LinksStack,
  SettingsStack,
  testStack,
  characterScreen,
},{
  tabBarOptions:{
    showLabel:false,
    keyboardHidesTabBar:true,
    style:{
      backgroundColor:"rgba(0,0,0,0)",
      borderTopWidth:0,
      borderColor:"rgba(0,0,0,0)",
      // position:"absolute",
    }
  }
});
tabNavigator.tabBarOptions={
  showLabel:false,
}
tabNavigator.path = '';

export default tabNavigator;
