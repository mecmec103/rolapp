export const characteristicsName = {
    strength: "Fue",
    const: "Con",
    size: "Tam",
    dexterity: "Des",
    looks: "Apa",
    inteligence: "Int",
    power: "Pod",
    education: "Edu",
    movement: "Mov",
    luck:"Sue",
    sanity:"Cor",
}