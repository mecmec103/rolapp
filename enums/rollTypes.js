export const rollTypes={
    STANDARD:0,
    ATRIBUTE:1,
    UPGRADE:2,
}
export const modifierTypes ={
    NONE:0,
    ADVANTAGE:1,
    DISADVANTAGE:2,
}