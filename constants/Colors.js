const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};

export const Color={
  border:"rgba(178,182,193,1)",
  yellow:"rgba(255,204,0,1)",
  green:"rgba(0,153,0,1)",
  red:"rgba(204,0,0,1)",
  black:"rgba(0,0,0,1)",
  blue:"rgba(6, 94, 209,1)"
}

export function colorOpacity(string,opacity){
  return string.slice(0,string.length-2)+opacity+")"
}