import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Color,colorOpacity} from "../../../constants/Colors";

export default function Characteristic(props) {

        return (
            <View style={styles.characteristicContainer}>
                <Text style={styles.text}>{props.name}</Text>
                <View style={styles.inputContainer}>
                    <TouchableOpacity>
                        <View style={styles.characteristicDataContainer}>
                            <View style={styles.mainValue}>
                                <Text style={styles.characteristicValue}>
                                    {props.value}
                                </Text>
                            </View>
                            <View style={styles.characteristicValueContainer}>
                                <View style={styles.halfValue}>
                                    <Text style={styles.characteristicValue}>
                                        {Math.trunc(props.value / 2)}
                                    </Text>
                                </View>
                                <View style={styles.bottom}>
                                    <Text style={[{lineHeight:18},styles.characteristicValue]}>
                                        {Math.trunc(props.value / 5)}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
}

const styles = StyleSheet.create({
    characteristicContainer: {
        display: "flex",
        flexDirection: "row",
        width: 105,
        height: 40,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 20,
        alignContent: "center"
    },
    text: {
        height: "100%",
        width: 30,
        lineHeight: 40,
        marginRight: 5,
        fontSize: 24,
        fontFamily:"corsiva",
    },
    inputContainer: {
        // shadowColor: "#000",
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
        width: 70,
        borderColor: colorOpacity(Color.border,0.35),
        borderWidth: 1,
        borderRadius: 3,
        borderTopRightRadius: 20,
        overflow:"hidden",
    },
    characteristicDataContainer: {
        display: "flex",
        flexDirection: "row",
        height: 40,
    },
    mainValue: {
        borderRightWidth: 1,
        borderColor: colorOpacity(Color.border,0.35),
        width: "50%",
        justifyContent: "center",
    },
    characteristicValue: {
        width: "100%",
        textAlign: "center",
        fontFamily:"corsiva",
    },
    characteristicValueContainer: {
        width: "50%",
        justifyContent: "center",
    },
    halfValue: {
        borderBottomWidth: 2,
        borderColor: colorOpacity(Color.border,0.35)
    },
})