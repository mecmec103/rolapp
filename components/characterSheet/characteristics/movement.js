import React, { useState } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Color, colorOpacity } from "../../../constants/Colors";
import { characteristicsName } from "../../../enums/characteristicsName";

export default function Movement(props) {

    const toggleModifier = (modifier) => {
        if (modifier == "advantage") {
            props.movement.advantage = !props.movement.advantage;
            props.movement.disAdvantage = false;
            props.onUpdate();
            setModifier({
                advantage: props.movement.advantage,
                disAdvantage: false
            })
        } else if (modifier == "disAdvantage") {
            props.movement.disAdvantage = !props.movement.disAdvantage;
            props.movement.advantage = false;
            props.onUpdate();
            setModifier({
                disAdvantage: props.movement.disAdvantage,
                advantage: false
            })
        }
    }
    let advantageContainer = styles.halfValue;
    let disAdvantageContainer;
    let advantageText = styles.modifier;
    let disAdvantageText = styles.modifier;
    const [modifier, setModifier] = useState({
        advantage: props.movement.advantage,
        disAdvantage: props.movement.disAdvantage
    });
    if (modifier.advantage == true) {
        advantageContainer = [styles.halfValue, styles.advantageActiveContainer];
        advantageText = styles.advantageActive;
    } if (modifier.disAdvantage == true) {
        disAdvantageContainer = [styles.halfValue, styles.disAdvantageActiveContainer];
        disAdvantageText = styles.disAdvantageActive;
    }

    return <View style={styles.characteristicContainer}>
        <Text style={styles.text}>{characteristicsName['movement']}</Text>
        <View style={styles.inputContainer}>
            <View style={styles.characteristicDataContainer}>
                <View style={styles.mainValue}>
                    <Text style={styles.characteristicValue}>
                        {props.movement.value}
                    </Text>
                </View>
                <View style={styles.characteristicValueContainer}>
                    <TouchableOpacity onPress={() => { toggleModifier("advantage") }}>
                        <View style={advantageContainer}>
                            <Text style={advantageText}>
                                +{1}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { toggleModifier("disAdvantage") }}>
                        <View style={[styles.bottom, disAdvantageContainer]}>
                            <Text style={[{ lineHeight: 18 }, disAdvantageText]}>
                                -{1}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    characteristicContainer: {
        display: "flex",
        flexDirection: "row",
        width: 105,
        height: 40,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 20,
        alignContent: "center"
    },
    text: {
        height: "100%",
        width: 30,
        lineHeight: 40,
        marginRight: 5,
        fontSize: 24,
        fontFamily:"corsiva",
    },
    inputContainer: {
        // shadowColor: "#000",
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
        width: 70,
        borderColor: colorOpacity(Color.border,0.35),
        borderWidth: 1,
        borderRadius: 3,
        borderTopRightRadius: 20,
        backgroundColor: "rgba(0,0,0,0)",
        overflow: "hidden",
    },
    characteristicDataContainer: {
        display: "flex",
        flexDirection: "row",
        height: 40,
    },
    mainValue: {
        borderRightWidth: 1,
        borderColor: colorOpacity(Color.border,0.35),
        width: "50%",
        justifyContent: "center",
    },
    characteristicValue: {
        width: "100%",
        textAlign: "center",
        fontFamily:"corsiva",
    },
    characteristicValueContainer: {
        width: "50%",
        justifyContent: "center",
    },
    halfValue: {
        borderBottomWidth: 2,
        borderColor: colorOpacity(Color.border,0.35)
    },
    modifier: {
        width: "100%",
        textAlign: "center",
        opacity: 0.25,
        fontFamily:"corsiva",
    },
    advantageActiveContainer: {
        backgroundColor: colorOpacity(Color.green, 0.35),

    },
    advantageActive: {
        width: "100%",
        textAlign: "center",
        color: Color.green,
        fontFamily:"corsiva",
        height:18
    },
    disAdvantageActiveContainer: {
        backgroundColor: colorOpacity(Color.red, 0.35),

    },
    disAdvantageActive: {
        width: "100%",
        textAlign: "center",
        color: Color.red,
        fontFamily:"corsiva",
    },
    bottom: {
        height: 20,
        lineHeight: 20
    }
})