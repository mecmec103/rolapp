import React from "react";
import { StyleSheet, View} from "react-native";
import { characteristicsName } from "../../../enums/characteristicsName";
import Characteristic from "./characteristic";
import Movement from "./movement";

export default function Characteristics(props) {
    let characteristics = [];
    let movement;
    for (let item in props.characteristics) {
        if (item != "movement"){
            characteristics.push({
                id: item,
                value: props.characteristics[item],
                name: characteristicsName[item]
            })
        }else{
           movement = {
               id:item,
               value:props.characteristics[item].value,
               advantage:props.characteristics[item].advantage,
               disAdvantage:props.characteristics[item].disAdvantage,
               name:characteristicsName[item],
           } 
        }
    }
    return (
        <View style={styles.characteristicsContainer}>
            {characteristics.map((el, i) => {
                return <Characteristic key={i} value={el.value} name={el.name}></Characteristic>
            })}
           <Movement onUpdate={props.onUpdate} movement = {props.characteristics.movement}></Movement>
        </View>)
}

const styles = StyleSheet.create({
    characteristicsContainer: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-between",
        paddingLeft:5,
        paddingRight:5
    },
})