import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Slider } from 'react-native-elements';
import { Entypo } from '@expo/vector-icons';
import { TouchableOpacity } from "react-native-gesture-handler";
import { Color } from "../../../constants/Colors";

export default function Mp(props) {
    const [mp, setMp] = useState(props.mp.current);
    const [width, setWidth] = useState(undefined);
    if(width==undefined){
        setWidth(2);
    }
    let varColor = Color.blue;
    const updateMp = (value) => {
        props.mp.current = value;
        props.onUpdate();
        setMp(props.mp.current);
    }
    const changeMp = ()=>{
    }

    const setSlider =   (e)=>{
        let relativePosition =e.nativeEvent.locationX /width;
        let equivalent = Math.trunc(relativePosition * props.mp.max);
        if(equivalent!=props.mp.current){
            updateMp(equivalent);
        }
    }
    return (
        <View style={styles.main}>
            <View style={styles.data}>
                <TouchableOpacity style={styles.titleContainer} onPress={changeMp}>
                    <Entypo name="shareable" size={30} style={{color:varColor}}></Entypo>
                    <Text style={styles.title}>PM</Text>
                </TouchableOpacity>
                <View style={styles.titleContainer}>
                    <Text style={[styles.currentMp,{color:varColor}]}>{mp}</Text>
                    <Text style={[styles.maxMp,{color:varColor}]}>/ {props.mp.max} Max.</Text>
                </View>
            </View>
            <View onLayout={(e)=>{setWidth(e.nativeEvent.layout.width)}} onTouchStart={setSlider}>
                <Slider
                    minimumValue={0}
                    maximumValue={props.mp.max}
                    minimumTrackTintColor={varColor}
                    maximumTrackTintColor="#f2f2f2"
                    trackStyle={styles.track}
                    animateTransitions={true}
                    thumbStyle={[styles.thumb,{backgroundColor:varColor}]}
                    animationType="spring"
                    step={1}
                    value={mp}
                    onValueChange={updateMp}
                />
            </View>
            <View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    track: {
        backgroundColor: "grey",
        height: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 1,

    },
    main:{
        paddingRight:10,
        width:"100%",
        height:70,
        display:"flex",
        justifyContent: "flex-end",
    },
    data:{
        display:"flex",
        justifyContent:"space-between",
        flexDirection:"row",
        position:"relative",
        alignContent:"center",
        alignItems:"center",
    },
    titleContainer:{
        height:30,
        display:"flex",
        flexDirection:"row",
        position:"relative",
        alignContent:"center",
        alignItems:"center",
        marginTop:10,
        marginBottom:-10
    },
    blood:{
        
    },
    currentMp:{
        fontSize:25,
        fontWeight:"bold"
    },
    maxMp:{
        fontSize:12,
        marginTop:15,
        marginLeft:5
    },
    thumb: {
        elevation:2,
        height:15,
        width:15
    },
    title:{
        fontSize:25,
        marginLeft:2,
        color:Color.blue,
    }
})