import React from "react";
import { StyleSheet, View } from "react-native";
import Characteristic from "../characteristics/characteristic";
import { characteristicsName } from "../../../enums/characteristicsName";

export default function Luck(props) {

    return (
        <View style={styles.main}>
            <Characteristic value={props.luck} name={characteristicsName["luck"]}></Characteristic>
        </View>
    )
}
const styles = StyleSheet.create({
    main: {
        width: "100%",
        height: 70,
        display: "flex",
        justifyContent: "flex-end",
    },
})