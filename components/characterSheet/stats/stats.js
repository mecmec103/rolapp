import React from "react";
import { StyleSheet, View } from "react-native";
// import Slider from "../Slider";
import Lp from "./lp";
import Mp from "./mp";
import Luck from "./luck";
import Sanity from "./sanity";


export default function Stats(props) {
    return (
        <View style={styles.statsContainer}>
            <View style={[styles.col,styles.firstCol]}>
                <Lp onUpdate={props.onUpdate} lp={props.stats.lp}></Lp>
                <Mp onUpdate={props.onUpdate} mp={props.stats.mp}></Mp>
            </View>
            <View style={[styles.col,styles.lastCol]}>
                <Luck  luck={props.stats.luck}></Luck>
                <Sanity sanity={props.stats.sanity}></Sanity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
   statsContainer:{
       display:"flex",
       flexDirection:"row",
       justifyContent:"space-between",
       paddingEnd:5,
       paddingStart:5,
   },
   col:{
       display:"flex",
       flexDirection:"column",
       alignContent:"center",
       alignItems:"center",
   },
   lastCol:{},
   firstCol:{
       width:"60%",
   }
})