import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Slider } from 'react-native-elements';
import { Entypo } from '@expo/vector-icons';
import { TouchableOpacity } from "react-native-gesture-handler";
import { Color } from "../../../constants/Colors";

function setColor(el){
    if(el.current>Math.trunc(el.max/2)){
        return Color.green;
    }else if(el.current<Math.trunc(el.max/3)) {
        return Color.red
    }else{
        return Color.yellow;
    }
}

export default function Lp(props) {
    const [lp, setLp] = useState(props.lp.current);
    const [isWounded,setWound] = useState(props.lp.isWounded);
    const [width, setWidth] = useState(undefined);
    if(width==undefined){
        setWidth(2);
    }
    let varColor = setColor(props.lp);
    const updateLp = (value) => {
        props.lp.current = value;
        props.onUpdate();
        setLp(props.lp.current);
    }
    const wound = ()=>{
        props.lp.isWounded = !isWounded;
        props.onUpdate();
        setWound(!isWounded)
    }

    const setSlider =   (e)=>{
        let relativePosition =e.nativeEvent.locationX /width;
        let equivalent = Math.trunc(relativePosition * props.lp.max);
        if(equivalent!=props.lp.current){
            updateLp(equivalent);
        }
    }
    return (
        <View style={styles.main}>
            <View style={styles.data}>
                <TouchableOpacity style={styles.titleContainer} onPress={wound}>
                    <Entypo name="drop" size={30} style={[styles.Blood,{color:isWounded? Color.red:Color.black}]}></Entypo>
                    <Text style={[styles.title,{color:isWounded? Color.red:Color.black}]}>PV</Text>
                </TouchableOpacity>
                <View style={styles.titleContainer}>
                    <Text style={[styles.currentLp,{color:varColor}]}>{lp}</Text>
                    <Text style={[styles.maxLp,{color:varColor}]}>/ {props.lp.max} Max.</Text>
                </View>
            </View>
            <View onLayout={(e)=>{setWidth(e.nativeEvent.layout.width)}} onTouchStart={setSlider}>
                <Slider
                    minimumValue={0}
                    maximumValue={props.lp.max}
                    minimumTrackTintColor={varColor}
                    maximumTrackTintColor="#f2f2f2"
                    trackStyle={styles.track}
                    animateTransitions={true}
                    thumbStyle={[styles.thumb,{backgroundColor:varColor}]}
                    animationType="spring"
                    step={1}
                    value={lp}
                    onValueChange={updateLp}
                />
            </View>
            <View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    track: {
        backgroundColor: "grey",
        height: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 1,

    },
    main:{
        paddingRight:10,
        width:"100%",
        height:50,
        marginTop:35,
        display:"flex",
        justifyContent: "flex-end",
    },
    data:{
        display:"flex",
        justifyContent:"space-between",
        flexDirection:"row",
        position:"relative",
        alignContent:"center",
        alignItems:"center",
    },
    titleContainer:{
        height:30,
        display:"flex",
        flexDirection:"row",
        position:"relative",
        alignContent:"center",
        alignItems:"center",
        marginTop:10,
        marginBottom:-10
    },
    blood:{
        
    },
    currentLp:{
        fontSize:25,
        fontWeight:"bold"
    },
    maxLp:{
        fontSize:12,
        marginTop:15,
        marginLeft:5
    },
    thumb: {
        elevation:2,
        height:15,
        width:15
    },
    title:{
        fontSize:25,
        marginLeft:2
    }
})

