import React from "react";
import { StyleSheet, View } from "react-native";
import Characteristic from "../characteristics/characteristic";
import { characteristicsName } from "../../../enums/characteristicsName";

export default function Sanity(props) {

    return (
        <View style={styles.main}>
            <Characteristic value={props.sanity.current} name={characteristicsName["sanity"]}></Characteristic>
        </View>
    )
}
const styles = StyleSheet.create({
    main: {
        width: "100%",
        height: 70,
        display: "flex",
        justifyContent: "flex-end",
    },
})
