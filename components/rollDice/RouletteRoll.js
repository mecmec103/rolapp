import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, Button } from "react-native";
import Animated, { Easing } from "react-native-reanimated";
import NumbersRoll from "./NumbersRoll";

export default function RouletteRoll(props) {
    const values = 25;
    const styles = StyleSheet.create({
        container: {
            overflow: "hidden",
            width: "100%",
            height: 30,
            marginLeft: "auto",
            marginRight: "auto",
            marginBottom: 10,
            position: "absolute",
        },
        animated: {
            width: "100%",
            textAlign: "center",
        },
        d4: {
            top: 42,
            right: 1
        },
        d6: {
            top: 41,
            right: 10
        },
        d8: {
            top: 14,
            right: 5
        },
        d10: {
            top: 14,
            left: 1,
        },
        dd10: {
            top: 14,
            left: 1
        },
        d12: {
            top: 26,
            left: 0
        },
        d20: {
            top: 25,
            left: 1
        },
        d100_d10: {
            top: 14,
            left: 18,
            transform: [{ rotate: "30deg" }]
        },
        d100_dd10: {
            top: 22,
            right: 11,
            transform: [{ rotate: "-10deg" }, { scale: 0.9 }]
        }

    })
    props.dice.values = values
    let start = new Animated.Value(1)
    let value = -30 * (props.dice.values - 1);
    const animate = ()=>{
        Animated.timing(
            start,
            {
                toValue: value,
                easing: Easing.ease,
                duration: 2000,
                delay: 500,
    
            }
        ).start((_) => {})
    }

    return (
        <View style={[styles.container, styles[props.dice.id]]}>
            <Animated.View style={[styles.animated, { translateY: start }]}>
                <NumbersRoll animate={animate} d100={props.d100} setSuccess={props.setSuccess} dice={props.dice} roll={props.roll} id={props.id}></NumbersRoll>
            </Animated.View>
        </View>
    )
}