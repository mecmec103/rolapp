import React, { useState } from "react";
import { StyleSheet, View, Image } from "react-native";
import RouletteRoll from "./RouletteRoll";


const styles = StyleSheet.create({
    diceContainer: {
        position: "relative",
        width: 70,
        height: 80,
        marginLeft:1,
        marginRight:1
    },
    diceContainerD12: {
        position: "relative",
        width: 80,
        height: 80,
        marginLeft:1,
        marginRight:1
    },
    diceContainerD100: {
        position: "relative",
        width: 80,
        height: 80,
        marginLeft:1,
        marginRight:1
    },
    diceImage: {
        aspectRatio: 1,
        width: "100%",
        height: "100%",
        maxHeight: 80,
        maxWidth: 70,
    },
    diceImageD12: {
        aspectRatio: 1,
        width: "100%",
        height: "100%",
        maxHeight: 80,
        maxWidth: 80,
    },
    diceImageD100: {
        aspectRatio: 1,
        width: "100%",
        height: "100%",
        maxHeight: 80,
        maxWidth: 80,
    }
})

const dices = {
    d4: {
        maxValue: 4,
        dice: "d4",
        step: 1,
        image: require("../../assets/images/dices/d4.png"),
    },
    d6: {
        maxValue: 6,
        dice: "d6",
        step: 1,
        image: require("../../assets/images/dices/d6.png"),
    },
    d8: {
        maxValue: 8,
        dice: "d8",
        step: 1,
        image: require("../../assets/images/dices/d8.png"),
    },
    d10: {
        maxValue: 10,
        dice: "d10",
        step: 1,
        image: require("../../assets/images/dices/d10.png"),
    },
    dd10: {
        maxValue: 10,
        dice: "dd10",
        step: 10,
        image: require("../../assets/images/dices/dd10.png"),
    },
    d12: {
        maxValue: 12,
        dice: "d12",
        step: 1,
        image: require("../../assets/images/dices/d12.png"),
    },
    d20: {
        maxValue: 20,
        dice: "d20",
        step: 1,
        image: require("../../assets/images/dices/d20.png"),
    },
    d100: {
        dice: "d100",
        image: require("../../assets/images/dices/d100.png"),
    },
}

function setStyleContainer(id){
    switch (id){
        case "d100":
            return styles.diceContainerD100;
        case "d12":
            return styles.diceContainerD12;
        default:
            return styles.diceContainer;
    }
}

function setStyleImage(id){
    switch (id){
        case "d100":
            return styles.diceImageD100;
        case "d12":
            return styles.diceImageD12;
        default:
            return styles.diceImage;
    }
}

export default function Dices(props) {
    dices[props.roll.dices[props.id].dice]['values'] = undefined;
    if (props.roll.dices[props.id].dice == "d100") {
        dices[props.roll.dices[props.id].dice]["d10"] = dices["d10"];
        dices[props.roll.dices[props.id].dice]["dd10"] = dices["dd10"];
        dices[props.roll.dices[props.id].dice]["d10"]["id"] = "d100_d10";
        dices[props.roll.dices[props.id].dice]["dd10"]["id"] = "d100_dd10";
    }
    let styleContainer = setStyleContainer(props.roll.dices[props.id].dice)
    let styleImage = setStyleImage(props.roll.dices[props.id].dice)
    if(props.roll.dices[props.id].dice!="d100"){
        dices[props.roll.dices[props.id].dice]['id']=props.roll.dices[props.id].dice;
        return (
            <View style={styleContainer}>
                <Image
                    style={styleImage}
                    source={dices[props.roll.dices[props.id].dice].image} />
                <RouletteRoll roll={props.roll} setSuccess={props.setSuccess} dice={dices[props.roll.dices[props.id].dice]} id={props.id} lenght={props.roll.dices.lenght}></RouletteRoll>
            </View>
        )
    }else{
        return (
            <View style={styleContainer}>
                <Image
                    style={styleImage}
                    source={dices[props.roll.dices[props.id].dice].image} />
                <RouletteRoll d100={1} roll={props.roll} setSuccess={props.setSuccess} dice={dices[props.roll.dices[props.id].dice].dd10} id={props.id} lenght={props.roll.dices.lenght}></RouletteRoll>
                <RouletteRoll d100={2} roll={props.roll} setSuccess={props.setSuccess} dice={dices[props.roll.dices[props.id].dice].d10} id={props.id} lenght={props.roll.dices.lenght}></RouletteRoll>
            </View>
        )
    }

}
