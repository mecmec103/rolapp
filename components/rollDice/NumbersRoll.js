import React from "react";
import { StyleSheet, Text } from "react-native";

export default function NumbersRoll(props) {
    const styles = StyleSheet.create({
        diceValue:{
            textAlign:"center",
            fontSize:16,
            color:"black",
            height:30
        }
    })
    const arr = [];
    let rand
    for(let i=0;i<props.dice.values;i++){
        rand = (Math.random()*10*props.dice.maxValue);
        if(props.dice.maxValue!=10){
            arr.push(Math.trunc(rand%props.dice.maxValue)+1)
        }else{
            arr.push(Math.trunc(rand%props.dice.maxValue)*props.dice.step)
        }
    }
    props.roll.dices[props.id].result = props.roll.dices[props.id].result==undefined? 0:props.roll.dices[props.id].result
    props.roll.dices[props.id].result+=arr[arr.length-1]
    props.animate(); 
    if((props.id+1)==props.roll.dices.length && ((props.d100!=null && props.d100==2) || props.d100==null)){
        props.setSuccess(props.roll);
    }
    return arr.map((el,i)=>{return <Text style={styles.diceValue} key={i}>{el}</Text>})
}