import React, { useState } from "react";
import { Modal, Alert, View, Button, StyleSheet } from "react-native";
import RollType from "./RollType";
import { rollTypes, modifierTypes } from "../../enums/rollTypes";

export default function Roll(props) {

    const styles = StyleSheet.create({
        modal: {
            minHeight: 400,
            width: "75%",
            minWidth: 330,
            backgroundColor: "white",
            marginLeft: "auto",
            marginRight: "auto",
            padding: 15,
            marginTop:"25%",
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,


        },
        rollContainer:{
            display:"flex",
            flexDirection:"row",
            flexWrap:"wrap",
            height: 200,
            width:"100%",
            justifyContent:"center",
        },
        button: {
            backgroundColor: "red"
        }
    })
    // const roll_back={
    //     attributeValue: 50,
    //     dices:[
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         },
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         },{
    //             dice:"d100",
    //             result:undefined,
    //         },
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         },
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         },
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         },
    //         {
    //             dice:"d100",
    //             result:undefined,
    //         }
    //         ],
    //     type:rollTypes.ATRIBUTE,
    //     modifier:modifierTypes.NONE,
    // }
    if(props.roll==undefined){
        return <View></View>;
    }
    let resultState;
    let resultValue;
    
    const setResult = (state,value)=>{
        for(let item of props.roll.dices){
            item.result=0;
        }
        resultState = state;
        resultValue = value;
        console.log("resultado (estado)=>", resultState,"resultado (valor)=>",resultValue);
    }

    return (
        <Modal
            style={styles.modal}
            animationType="fade"
            transparent={true}
            visible={props.isVisible}
            onRequestClose={() => { Alert.alert('El modal se serro de forma sensual') }}
            onShow={()=>{}}
        >
            <View style={styles.modal}>
                <View style={styles.rollContainer}>
                    <RollType 
                    roll={props.roll} 
                    resultState={resultState} 
                    resultValue={resultValue}
                    setResult={setResult}>
                    </RollType>
                </View>
                <View>
                    <Button style={styles.button} title="cerrar modal" onPress={props.onChange}>
                    </Button>
                </View>
            </View>
        </Modal>
    )
}