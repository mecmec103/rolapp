import React from "react";
import { StyleSheet, View } from "react-native";
import { rollTypes, modifierTypes } from "../../enums/rollTypes";
import { diceTypes } from "../../enums/diceTypes";
import { resultTypes } from "../../enums/resultTypes";
import Dices from "./Dices";



export default function RollType(props) {

    // console.log(props)
    const setSuccess=(roll)=>{
        for(let item of roll.dices){
            if(item.result==undefined){
                return;
            }if(item.result==0 && item.dice==diceTypes.d100){
                item.result=100;
            }
        }
        let auxResult = 0;
        switch (roll.type){
            case rollTypes.ATRIBUTE:
                let blunder = roll.attributeValue>=50? 99:94;
                blunder = roll.blunder!=undefined? roll.blunder:blunder;
                let dd10 = 0;
                let d10 = 0;
                let isZero=false;
                switch (roll.modifier){
                    case modifierTypes.ADVANTAGE:
                        dd10 =999999;
                        for(let item of roll.dices){
                            if(item.dice==diceTypes.d10){
                                d10=item.result;
                            }else{
                                dd10= item.result<dd10? item.result:dd10;
                            }
                        }
                        auxResult = dd10+d10;
                        if(auxResult==0){
                            dd10=9999999999;
                            for(let item of roll.dices){
                                if(item.dice==diceTypes.dd10 && item.result!=0){
                                    dd10= item.result<dd10? item.result:dd10;
                                }
                            }
                            auxResult = dd10+d10
                        }
                        break;
                    case modifierTypes.DISADVANTAGE:
                        
                        for(let item of roll.dices){
                            if(item.dice==diceTypes.d10){
                                d10=item.result;
                            }else{
                                if(item.result==0){
                                    isZero = true;
                                }
                                dd10= item.result>dd10? item.result:dd10;
                            }
                        }
                        auxResult = dd10 + d10;
                        if(d10==0 && isZero){
                            auxResult=100;
                        }
                        break;
                    case modifierTypes.NONE:
                        for(let item of roll.dices){
                            auxResult+=item.result;
                        }
                        break;
                }
                if(auxResult>blunder){
                    props.setResult(resultTypes.BLUNDER,auxResult);
                }else if(auxResult>roll.attributeValue){
                    props.setResult(resultTypes.FAILURE,auxResult);
                }else if(auxResult>roll.attributeValue/2){
                    props.setResult(resultTypes.SUCCES,auxResult);
                }else if(auxResult>roll.attributeValue/5){
                    props.setResult(resultTypes.GREAT_SUCCESS,auxResult);
                }else{
                    props.setResult(resultTypes.EXTREME_SUCCESS,auxResult);
                }
                break;
            case rollTypes.STANDARD:
                for(let item of roll.dices){
                    auxResult+=item.result;
                }
                props.setResult(resultTypes.NONE,auxResult)
                break;
            case rollTypes.UPGRADE:
                for(let item of roll.dices){
                    auxResult+=item.result;
                }
                if(auxResult>roll.attributeValue){
                    props.setResult(resultTypes.SUCCES,auxResult);
                }else{
                    props.setResult(resultTypes.FAILURE,auxResult);
                }
                break;
        }
    }
    return props.roll.dices.map((_,i)=><Dices key={i} roll={props.roll} setSuccess={setSuccess} id={i} ></Dices>)
}