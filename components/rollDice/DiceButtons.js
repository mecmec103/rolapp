import React from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import { arrayDices } from "../../enums/diceTypes";
import { TouchableOpacity } from "react-native-gesture-handler";
import { rollTypes } from "../../enums/rollTypes";
import { StackViewStyleInterpolator } from "react-navigation-stack";

const images = {
    d4: require("../../assets/images/dices/d4.png"),
    d6: require("../../assets/images/dices/d6.png"),
    d8: require("../../assets/images/dices/d8.png"),
    d10: require("../../assets/images/dices/d10.png"),
    dd10: require("../../assets/images/dices/dd10.png"),
    d12: require("../../assets/images/dices/d12.png"),
    d20: require("../../assets/images/dices/d20.png"),
    d100: require("../../assets/images/dices/d100.png"),
}

export default function DiceButtons(props) {

    const styles = StyleSheet.create({
        styleImageContainer:{
            width:100,
            height:140,
            marginTop:35,
        },
        styleImage:{
            aspectRatio:1,
            height:100,
            width:"auto",
            marginTop:10
        },
        styleImageD20:{
            height:120,
            width:100,
        },
        styleTouchableWrapper:{
            height:120,
            position:"relative"
        },
    })
    const rollDice = (el) =>{
        let roll = {
            type:rollTypes.STANDARD,
            dices:[
                {
                    dice:el,
                    result:0,
                }
            ]
        }
        props.rollDice(roll);
    }
    return arrayDices.map((el,i)=>{
    return  <View style={styles.styleImageContainer} key={i}>
                <TouchableOpacity style={styles.styleTouchableWrapper} onPress={()=>{rollDice(el)}}>
                    <Image
                    style={(el=="d20")? styles.styleImageD20:styles.styleImage}
                    source={images[el]}/>
                </TouchableOpacity>
            </View>})
}