import React from "react";
import { StyleSheet, View } from "react-native";
import CharacterMainDisplay from "./CharacterMainDisplay";




export default function CharactersDisplay(props) {
    console.log(props);
    let idx = props.characters.characters.findIndex(el=>el['active']==true);
    if(idx==-1){
        idx=0;
    }
    props.characters.characters.forEach((el,i) => {
        if(i!=idx){
           delete el['active'];
        }else{
            el['active']=true;
        }
    });
    console.log(props.characters.characters,idx);
    return (
    <View style={{flex:1}}>
        <CharacterMainDisplay character={props.characters.characters[idx]}></CharacterMainDisplay>
    </View>
    )
}