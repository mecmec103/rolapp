import React, { useState } from "react";
import { StyleSheet, View, Image, AsyncStorage,Text } from "react-native";
import * as MediaLibrary from 'expo-media-library';
import * as ImagePicker from 'expo-image-picker';
import { TouchableOpacity } from "react-native-gesture-handler";

const mask = require("../../assets/images/resources/CharacterMask.png")
const default1 = require("../../assets/images/resources/defaultCharacter1.png")
const default2 = require("../../assets/images/resources/defaultCharacter2.png")
const default3 = require("../../assets/images/resources/defaultCharacter3.png")

function setStockImage() {
    switch (Math.trunc((Math.random() * 100) % 3)) {
        case 0:
            return default1;
        case 1:
            return default2;
        case 2:
            return default3;
    }

}
export default function CharacterMainDisplay(props) {
    const [image, setImage] = useState({ uri: "noPic" });
    if (image.uri == "noPic") {
        AsyncStorage.getItem("characterImg" + props.character.id)
            .then(r => {
                if (r == undefined || r==null) {
                    setImage(setStockImage());
                } else {
                    setImage({ uri: r });
                }
            })
    }
    const pickImage = () => {
        ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [70, 88],
            quality: 1,
        }).then(r => {
            if (r != undefined && r.cancelled == false) {
                MediaLibrary.createAssetAsync(r.uri).then(img => {
                    MediaLibrary.saveToLibraryAsync(img.uri)
                        .then((_) => {
                            AsyncStorage.setItem("characterImg" + props.id, img.uri)
                                .then(() => {
                                    setImage({ uri: img.uri });
                                })
                        })
                })
            }

        })
    }
    return (
        <View>
            <View style={styles.characterContainer}>
                <View style={styles.imageContainer}>
                    <Image
                        imageStyle={styles.imageStyleProps}
                        style={styles.imageStyle}
                        source={image}></Image>
                </View>
                <TouchableOpacity style={styles.touchableMainContainer} onPress={(_) => { console.log("hola"), pickImage() }}>
                    <Image
                        style={styles.maskStyles}
                        source={mask}>
                    </Image>
                </TouchableOpacity >
            </View >
            <View style={styles.textContainer}>
                <Text style={styles.name}>{props.character.name}</Text>
            </View>
        </View>



    )
}

const styles = StyleSheet.create({
    characterContainer: {
        display: "flex",
        position: "absolute",
        top: 30,
        left: 20,
    },
    maskStyles: {
        width: 70,
        height: 88,
    },
    imageContainer: {
        position: "absolute",
        left: 2,
        top: 1,
        width: 66,
        height: 86,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
        overflow: "hidden",
    },
    imageStyle: {
        width: "100%",
        height: "100%",
    },
    textContainer:{
        position:"absolute",
        left:100,
        top:65
    },
    name:{
        fontFamily:"corsiva",
        fontSize:45,
        textTransform:"capitalize",
        color:"black"
    }
})