import firebase, { firestore } from "firebase"
import { AsyncStorage } from "react-native";
import { defaultCharacterSheet } from "../constants/defaultCharacterSheet";
require("firebase/firestore");
require("firebase/auth");


export class SheetManager {
    static userId;
    static hasChanges;
    static characters;

    static buildSheet = (characterId) => {
        return AsyncStorage.getItem("character" + characterId).then(r => {
            if (r == undefined) {
                return AsyncStorage.getItem("token").then(response => {
                    return firebase.firestore().collection("characters").doc(JSON.parse(response).id)
                        .collection("characterData").doc(characterId.toString()).get()
                        .then(data => {
                            if(data.data()==undefined){
                                return defaultCharacterSheet;
                            }
                            return AsyncStorage.setItem("character" + characterId, JSON.stringify(data.data()))
                                .then(r => {
                                    return getSheet(data.data());
                                })
                        })
                })
            } else {
                return getSheet(JSON.parse(r));
            }
        })
    }

    static saveSheet = (sheet,characterId)=>{
        return AsyncStorage.removeItem("character"+characterId)
            .then(()=>{
                return AsyncStorage.setItem("character"+characterId,JSON.stringify(sheet))
            })
    }

}

getSheet = (customData) => {
    let sheet = JSON.parse(JSON.stringify(defaultCharacterSheet));
    for (let main in defaultCharacterSheet) {
        switch (main) {
            case "stats" || "money":
                if (customData[main] != undefined) {
                    for (let item in defaultCharacterSheet[main]) {
                        if (customData[main][item] != undefined) {
                            for (let snap in defaultCharacterSheet[main][item]) {
                                if (customData[main][item][snap] != undefined) {
                                    sheet[main][item][snap] = customData[main][item][snap];
                                }
                            }
                        }
                    }
                }
                break;
            case "weapons":
                if (customData[main] != undefined && customData[main].length != 0) {
                    sheet[main] = sheet[main].concat(JSON.parse(JSON.stringify(customData[main].filter(el => !isInTheArray(sheet[main],el)))));
                }
                break;
            case "notes":
                sheet[main]=customData[main]
                break;
            case "characteristics":
                if (customData[main] != undefined) {
                    for (let item in customData[main]) {
                        if(item!="movement"){
                            sheet[main][item] = customData[main][item];
                        }else{
                            for(let snap in customData[main][item]){
                                sheet[main][item][snap] = customData[main][item][snap];
                            }
                        }
                    }
                }
                break;
            default:
                if (customData[main] != undefined) {
                    for (let item in customData[main]) {
                        sheet[main][item] = customData[main][item];
                    }
                }
                break;
        }
    }
    return sheet;
}

isInTheArray=(array,el)=>{
    for(let item of array){
        let isTheSame = true;
        for(let data in item){
            if(item[data]!=el[data]){
                isTheSame=false;
            }
        }
        if(isTheSame){
            return isTheSame
        }
    }
    return false;
}

