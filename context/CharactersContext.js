import React, {createContext, useState} from 'react';

export const ThemeContext = createContext();

export function ThemeContextProvider(){
    const [characters,setCharacters] = useState({});


    const updateCharacters=()=>{
       return AsyncStorage.getItem("token")
        .then(r=>{if(r!=null){
            let userId = JSON.parse(r).id;
           return AsyncStorage.getItem("characters")
                .then(localCharacter=>{
                    if(localCharacter==undefined || localCharacter==null){
                        firebase.firestore().collection("characters").doc(userId).get()
                                .then(r=>{
                                    if(r.data()!=undefined){
                                    AsyncStorage.setItem("characters",JSON.stringify(r.data()))
                                    setLoggin({characters:r.data(),isLogged:true});
                                    }else{
                                        let newCharacter = {
                                            "characters":[{"id":1,"name":""}],
                                            "last":1
                                        }
                                        AsyncStorage.setItem("characters",JSON.stringify(newCharacter.data()))
                                        setLoggin({characters:newCharacter,isLogged:true});
                                    }
                                })
                    }else{
                        setCharacters()
                        setLoggin({characters:JSON.parse(localCharacter),isLogged:true});
                    }
                })
        }})
    }






    return (
      <ThemeContext.Provider value={{...characters}}>
        {this.props.children}
      </ThemeContext.Provider>
    );
}
 
export default ThemeContextProvider;