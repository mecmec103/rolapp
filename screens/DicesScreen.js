import React, { useState }  from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Roll from '../components/rollDice/Roll';
import DiceButtons from '../components/rollDice/DiceButtons';
import { FlatList, ScrollView } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
    container:{
        display:"flex",
        flexDirection:"row",
        flexWrap:"wrap",
        justifyContent:"space-evenly"
    },
    image:{
        position:"absolute",
        top:170,
        left:180,
        width:30,
        height:30
    }
})
const sign = require("../assets/images/elderSign.png")
export default function DicesScreen() {
    /**
     * Go ahead and delete ExpoConfigView and replace it with your content;
     * we just wanted to give you a quick view of your config.
     */
    const [roll,setRoll] = useState(undefined);
    const [isVisible,setVisibility] = useState(false);
    const rollDice = (roll) =>{
        setRoll(roll);
        setVisibility(true);
    }
    const hideModal= ()=>{
        setVisibility(false);
    }
    return (
        <ScrollView>
            <Image
            source={sign}
            style={styles.image}
            ></Image>
            <View style={styles.container}>
            <Roll roll={roll} isVisible={isVisible} onChange={hideModal}></Roll>
            <DiceButtons rollDice={rollDice}></DiceButtons>
            <DiceButtons rollDice={rollDice}></DiceButtons>
            <DiceButtons rollDice={rollDice}></DiceButtons>
        </View>
        </ScrollView>

    );
}

DicesScreen.navigationOptions = {
    header:null
};