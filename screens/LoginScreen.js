import React, { useState } from "react";
import { StyleSheet, View, Text, Button } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { AsyncStorage } from 'react-native';

import firebase from "firebase"
require("firebase/firestore");
require("firebase/auth");


export default function LoginScreen(props) {
    const text =( <View>
        <Text style={styles.title}>Esta es una aplicación para nada sospechosa</Text>
        <Text style={styles.subtitle}>Asi que porfavor, "loggeate"</Text>
        </View>)


    const errorText = <Text style={styles.error}>Mira hij@ de put@, te voy a matar como no me introduzcas unos datos adecuados</Text>;
    const [error, setError] = useState(false)
    let user = ""
    let pass = ""
    
    const logIn = () => {
        firebase.auth().createUserWithEmailAndPassword(user,pass)
            .then(r =>{
                uploadLogIn(r.user.uid);
            })
            .catch(_ => {
                firebase.auth().signInWithEmailAndPassword(user,pass)
                    .then(r=>{
                        uploadLogIn(r.user.uid);
                    })
                    .catch((_)=>{setError(true)})
            })
    }

    const uploadLogIn=(uid)=>{
    firebase.firestore().collection("users").doc(uid).get()
        .then(response=>{
            let data = response.data();
            if(data==undefined){
                firebase.firestore().collection("users").doc("last").get()
                    .then(lastUser=>{
                        let last = lastUser.data().id
                        let idNewUser = ("0"+(parseInt(last)+1)).slice(-2);
                        firebase.firestore().collection("users").doc(uid).set({
                            id:idNewUser
                        })
                        .then(()=>{
                            firebase.firestore().collection("users").doc("last").update({
                                id:idNewUser
                            }).then(()=>{
                                saveLogIn({
                                    user:user,
                                    pass:pass,
                                    id:idNewUser,
                                })
                            })
                        })
                    })
            }else{
                saveLogIn({
                    user:user,
                    pass:pass,
                    id:data.id,
                })
            }
        });
    }

    const saveLogIn=(data)=>{
        AsyncStorage.setItem("token",JSON.stringify(data)).then(()=>{
            props.setLoggin(true)
        })
    }

    return (
        <View style={styles.mainWrapper}>
            <View style={styles.logginBox}>
                <View>
                    {error ? errorText : text}
                    <TextInput keyboardType="email-address" onChangeText={t => { user = t }} placeholder="Mi mail?... tengo miedo" style={styles.input}></TextInput>
                    <TextInput secureTextEntry={true} onChangeText={t => { pass = t }} placeholder="Mi contraseña?...(por favor no me mates)" style={styles.input}></TextInput>
                    <Text style={styles.infoPass}>La contraseña minimo de 6 digitos</Text>
                    <TouchableOpacity onPress={logIn} style={styles.button}>
                        <Text style={styles.buttonText}>Estoy seguro de que esto, no es un error (log in)</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    mainWrapper: {
        backgroundColor: "#bcc2be",
        height: "100%",
        width: "100%",
        justifyContent: "center",
    },
    logginBox: {
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        width: "90%",
        height: 325,
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        padding: 20
    },
    title: {
        textAlign: "center",
        marginLeft: "auto",
        marginRight: "auto",
        fontSize: 25
    },
    subtitle: {
        marginTop: 5,
        color: "#bcc2be",
        width: "100%",
        textAlign: "center",
    },
    input: {
        width: "100%",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.015,
        shadowRadius: 3.84,

        elevation: 1.1,
        borderRadius: 5,
        marginTop: 10,
        padding: 5,
        paddingLeft: 10,
    },
    button: {
        marginTop: 20,
        borderColor: "#7aa7c7",
        backgroundColor: "#e1ecf4",
        padding: 10,
        width: 190,
        marginLeft: "auto",
        marginRight: "auto",
        borderRadius: 10,
        shadowColor: "#39739d",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 3.84,

        elevation: 2.2,
    },
    buttonText: {
        color: "#39739d",
        width: "100%",
        textAlign: "center",
    },
    error: {
        textAlign: "center",
        marginLeft: "auto",
        marginRight: "auto",
        fontSize: 25,
        color:"red"
    },
    infoPass:{
        marginTop: 5,
        color: "#bcc2be",
        marginLeft:3
    }
})