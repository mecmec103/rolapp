import React, { useState } from "react";
import { StyleSheet, View,ImageBackground } from "react-native";
import Characteristics from "../components/characterSheet/characteristics/characteristics";
import Stats from "../components/characterSheet/stats/stats";
import { SheetManager } from "../components/SheetManager";

const background = require("../assets/images/resources/background.jpg");
export default function CharacterScreen(props) {
    const updateState=()=>{
        SheetManager.saveSheet(sheet,1).then(()=>{
            setSheet(sheet);
        })
    }
    const styles = StyleSheet.create({
    })
    const [sheet,setSheet] = useState(undefined);
    if(sheet==undefined){
        SheetManager.buildSheet(1).then(r=>{setSheet(r)});
        return<View></View>
    }else{
        return (
                <View style={{backgroundColor:"rgba(0,0,0,0)"}}>
                    {/* <Stats stats={sheet.stats} onUpdate={updateState}></Stats>
                    <Characteristics onUpdate={updateState} characteristics={sheet.characteristics}></Characteristics> */}
                </View>
        )
    }
}

CharacterScreen.navigationOptions={
    header:null,
    title:"vista general"
}